1
00:00:00,200 --> 00:00:04,500
Le satellite suit dans un premier temps l'orbite elliptique verte.

2
00:00:05,100 --> 00:00:9,200
Au plus loin de la Terre, le transfert commence et les moteurs s'allument.

3
00:00:10,000 --> 00:00:15,200
Le satellite a pour objectif de rejoindre l'orbite circulaire qui apparait en bleu à l'écran,

4
00:00:15,500 --> 00:00:21,600
en un temps fixé à 1,5 fois le temps minimal, c'est-à-dire environ 90 heures, 

5
00:00:21,800 --> 00:00:24,300
en consommant le moins de carburant possible.

6
00:00:25,000 --> 00:00:29,100
Ce temps minimal de transfert dépend de la poussée maximale des moteurs 

7
00:00:29,200 --> 00:00:33,500
que l'on peut voir à gauche de l'écran être fixée à 20N.

8
00:00:35,000 --> 00:00:39,500
Revenons sur le satellite. Nous pouvons remarquer que sa stratégie alterne

9
00:00:39,700 --> 00:00:44,100
entre des phases de poussée à pleine puissance et des phases à moteurs éteints.

10
00:00:45,000 --> 00:00:51,300
La flèche rouge ancrée sur le satellite nous indique la direction de poussée
quand les moteurs sont allumés.

11
00:00:52,500 --> 00:00:58,100
Le satellite profite de la vitesse que lui procure l'attraction terrestre 
lorsqu'il est proche de la Terre

12
00:00:58,500 --> 00:01:06,100
et profite d'une plus faible attraction pour allumer ses moteurs et 
régler sa vitesse lorsqu'il est loin de celle-ci.

13
00:01:09,900 --> 00:01:14,600
Le satellite fait un dernier ajustement avant de rejoindre l'orbite finale

14
00:01:15,000 --> 00:01:18,200
pour être à la bonne distance de la Terre et avoir la bonne vitesse, 

15
00:01:18,800 --> 00:01:23,500
ce qui lui permet ensuite de maintenir son orbite malgré les moteurs éteints.

16
00:01:25,700 --> 00:01:28,800
Nous pouvons finalement remarquer, comme indiqué en bas à gauche de l'écran, 

17
00:01:29,200 --> 00:01:36,000
que malgré une durée de transfert plus grande que celle du problème à temps minimal, 
le satellite a consommé moins de carburant.


