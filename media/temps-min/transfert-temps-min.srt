1
00:00:00,200 --> 00:00:04,500
Le satellite suit dans un premier temps l'orbite elliptique verte.

2
00:00:05,100 --> 00:00:9,200
Au plus loin de la Terre, le transfert commence 
et les moteurs s'allument.

3
00:00:10,000 --> 00:00:14,600
Le satellite a pour objectif de rejoindre l'orbite
circulaire qui apparait en bleu,

4
00:00:15,200 --> 00:00:17,300
le plus rapidement possible.

5
00:00:18,000 --> 00:00:23,850
La stratégie consiste donc à manoeuvrer tout en
gardant une poussée maximale à chaque instant.

6
00:00:24,500 --> 00:00:28,100
Cette poussée étant fixée ici à 20N.

7
00:00:29,700 --> 00:00:34,600
La flèche rouge ancrée sur le satellite 
indique la direction de poussée.

8
00:00:35,000 --> 00:00:38,800
Nous pouvons voir ces deux coordonnées 
en haut à gauche de l'écran.

9
00:00:39,700 --> 00:00:43,200
Revenons sur le satellite et remarquons sur le dernier tour

10
00:00:43,300 --> 00:00:46,300
du transfert qu'il a changé le sens de rotation de

11
00:00:46,400 --> 00:00:49,200
son attitude pour amorcer la phase finale.

12
00:00:49,800 --> 00:00:53,200
Nous pouvons voir en bas à gauche de l'écran que la vitesse

13
00:00:53,300 --> 00:00:57,100
du satellite diminue légèrement à l'approche de l'orbite cible :

14
00:00:57,400 --> 00:00:58,800
le satellite freine.

15
00:00:59,500 --> 00:01:04,050
Ca y'est, le transfert est maintenant fini après près de 60 heures.

15
00:01:04,600 --> 00:01:13,200
Le satellite se trouve à la bonne distance de la Terre et 
se déplace à la bonne vitesse, ce qui lui permet de maintenir
cet orbite malgré les moteurs éteints.
