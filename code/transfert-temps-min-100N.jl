using MINPACK # fsolve
using LinearAlgebra # norm

include("commun/flows.jl");
include("commun/plottings.jl");

x0     = [-42272.67, 0, 0, -5796.72] # état initial
μ      = 5.1658620912*1e12
rf     = 42165.0 ;
F_max  = 100.0
γ_max  = F_max*3600.0^2/(2000.0*10^3)
t0     = 0.0
rf3    = rf^3  ;
α      = sqrt(μ/rf3);

# Maximizing control
function control(p)
    u    = zeros(eltype(p),2)
    u[1] = p[3]*γ_max/norm(p[3:4])
    u[2] = p[4]*γ_max/norm(p[3:4])
    return u
end;

# Maximized Hamiltonian
function hfun(x, p)
    u   = control(p)
    nor = norm(x[1:2]); nor3 = nor^3
    h   = p[1]*x[3] + p[2]*x[4] + p[3]*(-μ*x[1]/nor3 + u[1]) + p[4]*(-μ*x[2]/nor^3 + u[2])
    return h
end;

# Flow
z = Flow(Hamiltonian(hfun))

# Données pour la trajectoire durant le transfert
mutable struct Transfert
    duration
    initial_adjoint
end

# Fonction de tir
function shoot(p0, tf)
    
    # Integration
    xf, pf = z(t0, x0, p0, tf)
    
    # Conditions
    s = zeros(eltype(p0), 5)
    s[1] = norm(xf[1:2]) - rf
    s[2] = xf[3] + α*xf[2]
    s[3] = xf[4] - α*xf[1]
    s[4] = xf[2]*(pf[1] + α*pf[4]) - xf[1]*(pf[2] - α*pf[3])
    s[5] = hfun(xf, pf) - 1
    
    return s

end;

# Initial guess
ξ_guess = [1.0323e-4, 4.915e-5, 3.568e-4, -1.554e-4, 13.4]   # for F_max = 100N
#ξ_guess = [-0.0013615, -7.34989e-6, -5.359923e-5, -0.00858271, 59.8551668] # for F_max = 20N

# Solve
foo(ξ) = shoot(ξ[1:4], ξ[5])
jfoo(ξ) = ForwardDiff.jacobian(foo, ξ)
foo!(s, ξ) = ( s[:] = foo(ξ); nothing )
jfoo!(js, ξ) = ( js[:] = jfoo(ξ); nothing )

nl_sol = fsolve(foo!, jfoo!, ξ_guess, show_trace=true); println(nl_sol)

# Retrieves solution
if nl_sol.converged
    p0_sol = nl_sol.x[1:4]
    tf_sol = nl_sol.x[5]
    transfert_data = Transfert(tf_sol, p0_sol);
else
    error("Not converged")
end

