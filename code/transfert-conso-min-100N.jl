using MINPACK # fsolve
using LinearAlgebra # norm

include("commun/flows.jl")
include("commun/plottings.jl")
include("commun/homotopy.jl")

m0     = 2000
x0 = [-42272.67, 0, 0, -5796.72, m0, 0]

F_max_100 = 100
tf_min_100 = 13.40318195708344
tf_100    = 20 # 1.5*tf_min_100

ε_init_1e0    = 1
p0_guess_100_1e0 = [  0.01318593990958066
0.0030896111721221775
0.02253103713204917
0.002206083679948253
-0.24017664378556397
0.0]

ε_init_1e_3    = 5e-3
p0_guess_100_1e_3 = [0.0012977929824425805, 
0.00032589568022940377, 
0.0023765992752143887, 
-0.00010621859791207892, 
-0.025235832339334786, 
1.3355190947675691e-14]

# ------------------------------
# init
ε_init_100 = ε_init_1e_3
p0_guess_100 = p0_guess_100_1e_3
# ε_fin
ε_fin = 5e-3
# ------------------------------

#
nx = size(p0_guess_100, 1)
Th(F) = F*3600.0^2/(10^3) # u_max
#u_max = F_max*3600.0^2/(10^3)
#γ_max = F_max*3600.0^2/(m0*10^3)

μ      = 398600.47 * 3600^2
rf     = 42165;
rf3    = rf^3
α      = sqrt(μ/rf3)
t0     = 0
β      = 0
tol    = 1e-10;

# Hamiltonian
# beta = 0 so we do not take it into account
#
function control(x, p, ε, u_max)
    φ = [p[3], p[4]]; nφ = norm(φ)
    m = x[5]
    ψ = -1.0 + (u_max/m)*nφ + p[6] - p[5]*β*u_max
    α = (ψ - 2ε + √(ψ^2 + 4ε^2))/(2ψ)
    u = (α/nφ)*φ
    return u
end

H(x, p, u, ε, u_max) = (-norm(u) + ε*(log(norm(u))+log(1.0-norm(u)))
                        + p[1]*x[3]
                        + p[2]*x[4]
                        + p[3]*(-μ*x[1]/norm(x[1:2])^3+(u_max/x[5])*u[1])
                        + p[4]*(-μ*x[2]/norm(x[1:2])^3+(u_max/x[5])*u[2])
                        - p[5]*β*u_max*norm(u)
                        + p[6]*norm(u)); # on ajoute la conso dans le hamiltonien: x[6]

H(x, p, ε, u_max) = H(x, p, control(x, p, ε, u_max), ε, u_max) 

# Flow
f = Flow(Hamiltonian(H));

# Fonction de tir
function shoot(p0, tf, ε, u_max)
    
    # Integration
    xf, pf = f(t0, x0, p0, tf, ε, u_max, abstol=tol, reltol=tol)
    
    # Conditions
    s = zeros(eltype(p0), 6)
    s[1] = norm(xf[1:2]) - rf
    s[2] = xf[3] + α*xf[2]
    s[3] = xf[4] - α*xf[1]
    s[4] = xf[2]*(pf[1] + α*pf[4]) - xf[1]*(pf[2] - α*pf[3])
    s[5] = pf[5]
    s[6] = pf[6]
    
    return s

end;

ε_init = ε_init_100
p0_first_shoot = p0_guess_100
tf_first_shoot = tf_100
Fm_first_shoot = F_max_100
tf_min_first_shoot = tf_min_100

# Initial guess
ξ_guess  = p0_first_shoot #+ 1e-4.*(-1.0 .+ (2.0 .* rand(Float64, 6)))

# Solve
S(ξ) = shoot(ξ, tf_first_shoot, ε_init, Th(Fm_first_shoot)) # on fixe les valeurs des paramètres
jS(ξ) = ForwardDiff.jacobian(S, ξ)
S!(s, ξ) = ( s[:] = S(ξ); nothing )
jS!(js, ξ) = ( js[:] = jS(ξ); nothing )

println("Initial value of shooting:\n", S(ξ_guess), "\n\n")

nl_sol = fsolve(S!, jS!, ξ_guess, show_trace=true, tol=1e-8); println(nl_sol)

# Retrieves solution
if nl_sol.converged
    p0_sol_first_shoot = nl_sol.x[1:nx]
else
    error("Not converged")
end

# homotopy
Hom(p0, ε) = shoot(p0, tf_first_shoot, ε, Th(Fm_first_shoot))
println("Initial value of homotopy:\n", Hom(p0_sol_first_shoot, ε_init), "\n\n")

if ε_init > ε_fin

    p = Path(Hom); 
    ε₀ = ε_init; ε₁ = ε_fin; path_ε = p(p0_sol_first_shoot, ε₀, ε₁, abstol=1e-10, reltol=1e-10, ftol_proj=1e-6);

    p0_guess_ε   = path_ε[1:nx, end]
    ε_final      = ε₁
    F_max_final  = Fm_first_shoot
    tf_final     = tf_first_shoot
    tf_min_final = tf_min_first_shoot
    
else

    p0_guess_ε   = p0_sol_first_shoot
    ε_final      = ε_init
    F_max_final  = Fm_first_shoot
    tf_final     = tf_first_shoot
    tf_min_final = tf_min_first_shoot

end

p0_sol_final = p0_guess_ε

# affichage
ode_sol = f((t0, tf_final), x0, p0_sol_final, ε_final, Th(F_max_final), abstol=tol, reltol=tol)
nn = size(ode_sol.t, 1)
uu = zeros(nn, 1)
nx = size(x0, 1)
for j in 1:nn
    uu[j] = norm(control(ode_sol[1:nx, j], ode_sol[nx+1:2nx, j], ε_final, Th(F_max_final)))
end
uu = uu.*F_max_final
println("consommation = ", ode_sol[6, end]/tf_min_final)
pp = plot(ode_sol.t, uu, xlabel = "t", ylabel = "||u||", legend=false)

plot(pp)
