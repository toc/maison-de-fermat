# Transfert orbital autour de la Terre d'un satellite 

Vous trouverez dans les répertoires `media/temps-min` et `media/conso-min` les fichiers 
aux extensions suivantes :

* `.aup3` : le fichier audio `Audacity` que l'on peut modifier, qui contient la description audio
du transfert orbital ;
* `mp3` : le fichier audio `.aup3` converti ;
* `.gif` : le fichier `GIF` du transfert orbital ;
* `.mp4` : la vidéo du transfert orbital avec/sans audio ;
* `.srt` : les sous-titres du transfert orbital, que l'on peut donc joindre à la vidéo.

Pour avoir un aperçu, cliquer sur les liens suivants :
* [transfert temps minimal](https://gitlab.irit.fr/toc/maison-de-fermat/-/raw/master/media/temps-min/transfert-temps-min-audio.mp4)
* [transfert conso minimale](https://gitlab.irit.fr/toc/maison-de-fermat/-/raw/master/media/conso-min/transfert-conso-min-audio.mp4)
